
#+OPTIONS: toc:nil num:nil
#+STARTUP: showall indent
#+STARTUP: hidestars

#+BEGIN_EXPORT html
---
layout: post
title: "A python script for key binding in gnome"
date:   2018-05-07
categories: [linux,productivity]
tags: [python]
---
#+END_EXPORT

If you want to leave your mouse alone as often as possible, you can configure personal shortkeys for your favorite programs. In Gnome, you can use your GUI, but I found useful this script for scripting these keybinding process.

#+BEGIN_EXAMPLE sh
# Usage
# python3 /path/to/script.py 'a personal name' 'a_command' '<Alt>7'
#+END_EXAMPLE

Here after, some of basic 'special' keys of your keyboard.


|                 |                    <r> |
| Super      key: |                <Super> |
| Control    key: | <Primary> or <Control> |
| Alt        key: |                  <Alt> |
| Shift      key: |                <Shift> |
| numbers:        |                      1 |
| Spacebar:       |                  space |
| Slash      key: |                  slash |

Here are my main keybindind configuration

#+BEGIN_EXAMPLE sh
python3 /path/to/script.py 'open emacs' 'emacs' '<Control>e'
python3 /path/to/script.py 'open terminal' 'gnome-terminal' '<Control>t'
python3 /path/to/script.py 'open mailbox' 'evolution' '<Control>m'
python3 /path/to/script.py 'open web browser' 'sensible-browser' '<Control>w'
#+END_EXAMPLE

#+BEGIN_EXAMPLE python
#!/usr/bin/env python3
import subprocess
import sys

# defining keys & strings to be used
key = "org.gnome.settings-daemon.plugins.media-keys custom-keybindings"
subkey1 = key.replace(" ", ".")[:-1]+":"
item_s = "/"+key.replace(" ", "/").replace(".", "/")+"/"
firstname = "custom"

# get the current list of custom shortcuts
get = lambda cmd: subprocess.check_output(["/bin/bash", "-c", cmd]).decode("utf-8")
current = eval(get("gsettings get "+key))

# make sure the additional keybinding mention is no duplicate
n = 1
while True:
    new = item_s+firstname+str(n)+"/"
    if new in current:
        n = n+1
    else:
        break

# add the new keybinding to the list
current.append(new)

# create the shortcut, set the name, command and shortcut key
cmd0 = 'gsettings set '+key+' "'+str(current)+'"'
cmd1 = 'gsettings set '+subkey1+new+" name '"+sys.argv[1]+"'"
cmd2 = 'gsettings set '+subkey1+new+" command '"+sys.argv[2]+"'"
cmd3 = 'gsettings set '+subkey1+new+" binding '"+sys.argv[3]+"'"

for cmd in [cmd0, cmd1, cmd2, cmd3]:
    subprocess.call(["/bin/bash", "-c", cmd])
#+END_EXAMPLE


* Source
[[https://askubuntu.com/questions/597395/how-to-set-custom-keyboard-shortcuts-from-terminal?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa]]
