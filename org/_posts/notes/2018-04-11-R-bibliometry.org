#+OPTIONS: toc:nil num:nil
#+STARTUP: showall indent
#+STARTUP: hidestars

#+BEGIN_EXPORT html
---
layout: post
title:  "Bibliometry"
subtitle: "#R"
date:   2018-04-11
categories: [data-sciences]
tags: [R]
---
#+END_EXPORT

You will find a method for extracting the abstracts of papers selected on a precise query formula. My example focus on some agroecological papers in Plos One journal.


* Installation

** Package /fulltext/ 
Source : [https://ropensci.org/tutorials/fulltext_tutorial/]

The tutorial is a good entry point. 

** Dependencies

The package pdftools depends on libpoppler-cpp-dev

#+BEGIN_SRC sh
# in Debian
sudo apt-get install libpoppler-cpp-dev
#+END_SRC

* Write your query and get information
#+BEGIN_SRC R
library("fulltext")

##  
myquery <- "(((everything:agroecology) 
            AND everything:farm*) 
            AND everything:practices) 
            AND publication_date:[2017-01-01T00:00:00Z TO 2018-01-01T23:59:59Z]"

ae <- ft_search(query = myquery, from = 'plos',limit=100)

## by default the field 'data' is null. Fill it with
ft.ae.data <- ft.ae %>% ft_collect()


#+END_SRC

* Extract abstracts and cat them in files
#+BEGIN_SRC R

library("xml2")

## using a loop --
## TODO maybe a lapply with function could be better

for (doi in names(ft.ae.data$plos$data$data)) {
    abstract <- xml_text(
         xml_find_all(
             ft.ae.data$plos$data$data[[doi]],
             "//article-meta//abstract"
             )
         )
    cat(abstract, '\n\n')
    ## cat(abstract,file=paste0("./data-output/",doi".txt"))
    Sys.sleep(1)
}
#+END_SRC

